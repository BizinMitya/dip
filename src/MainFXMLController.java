import com.sun.istack.internal.Nullable;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;
import org.jtransforms.fft.DoubleFFT_2D;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.function.BiFunction;

/**
 * Created by Dmitriy on 09.04.2017.
 */
public class MainFXMLController implements Initializable {
    @FXML
    private MenuItem maskMenuItem;
    @FXML
    private Pane pane;
    @FXML
    private MenuBar menuBar;
    @FXML
    private MenuItem openMenuItem;
    @FXML
    private MenuItem lowPassGaussMenuItem;
    @FXML
    private MenuItem lowPassButterworthMenuItem;
    @FXML
    private MenuItem highPassGaussMenuItem;
    @FXML
    private MenuItem highPassButterworthMenuItem;
    @FXML
    private MenuItem rejectGaussMenuItem;
    @FXML
    private MenuItem rejectButterworthMenuItem;
    @FXML
    private MenuItem alphaMenuItem;
    @FXML
    private MenuItem nMenuItem;
    @FXML
    private MenuItem wMenuItem;
    @FXML
    private MenuItem noiseOfGaussMenuItem;
    @FXML
    private MenuItem noiseOfRayleighMenuItem;
    @FXML
    private MenuItem noiseOfErlangMenuItem;
    @FXML
    private MenuItem noiseOfExpMenuItem;
    @FXML
    private MenuItem noiseOfUniformMenuItem;
    @FXML
    private MenuItem arithmeticMeanAntiNoiseMenuItem;
    @FXML
    private MenuItem harmonicMeanAntiNoiseMenuItem;
    @FXML
    private MenuItem noiseMenuItem;
    @FXML
    private MenuItem brightnessMenuItem;
    @FXML
    private MenuItem contrastMenuItem;
    @FXML
    private MenuItem setBrightnessAndContrastMenuItem;
    @FXML
    private MenuItem colorSchemeMenuItem;
    @FXML
    private MenuItem setColorSchemeMenuItem;

    private Slider alphaSlider;
    private Slider nSlider;
    private Slider wSlider;
    private Slider noiseSlider;
    private Slider widthMaskSlider;
    private Slider heightMaskSlider;
    private Slider brightnessSlider;
    private Slider contrastSlider;
    private Slider redSlider;
    private Slider greenSlider;
    private Slider blueSlider;

    @FXML
    private ImageView imageView;

    private File currentImage;

    private BiFunction<Double, Double, Double> lowPassGauss = (d, d0) -> Math.exp(-d * d / (2 * d0 * d0));
    private BiFunction<Double, Double, Double> lowPassButterworth = (d, d0) -> 1 / (1 + Math.pow(d / d0, 2 * nSlider.getValue()));
    private BiFunction<Double, Double, Double> highPassGauss = (d, d0) -> 1 - Math.exp(-d * d / (2 * d0 * d0));
    private BiFunction<Double, Double, Double> highPassButterworth = (d, d0) -> 1 / (1 + Math.pow(d0 / d, 2 * nSlider.getValue()));
    private BiFunction<Double, Double, Double> rejectGauss = (d, d0) -> 1 - Math.exp(-(((d * d - d0 * d0) / (d * wSlider.getValue())) * ((d * d - d0 * d0) / (d * wSlider.getValue()))) / 2);
    private BiFunction<Double, Double, Double> rejectButterworth = (d, d0) -> 1 / (1 + Math.pow(d * wSlider.getValue() / (d * d - d0 * d0), 2 * nSlider.getValue()));

    private Noise noiseOfGauss = (z, m, d) -> Math.exp(-(z - m) * (z - m) / (2 * d)) / (Math.sqrt(2 * Math.PI * d));
    private Noise noiseOfRayleigh = (z, m, d) -> {
        double p = 0;
        double a = m - Math.sqrt(Math.PI * d / (4 - Math.PI));
        if (z >= a) {
            double b = 4 * d / (4 - Math.PI);
            p = 2 * (z - a) * Math.exp(-(z - a) * (z - a) / b) / b;
        }
        return p;
    };
    private Noise noiseOfErlang = (z, m, d) -> {
        double p = 0;
        if (z >= 0) {
            double a = m / d;
            int b = (int) (m * m / d);
            p = Math.pow(a, b) * Math.pow(z, b - 1) * Math.exp(-a * z) / fact(b - 1);
        }
        return p;
    };
    private Noise noiseOfExp = (z, m, d) -> {
        double p = 0;
        if (z >= 0) {
            double a = 1 / m;
            d = a * Math.exp(-a * z);
        }
        return p;
    };
    private Noise noiseOfUniform = (z, m, d) -> {
        double b1 = m + Math.sqrt(3 * d),
                b2 = m - Math.sqrt(3 * d),
                a1 = 2 * m - b1,
                a2 = 2 * m - b2;
        if (a1 <= z && z <= b1 && a1 != b1) return 1 / (b1 - a1);
        if (a2 <= z && z <= b2 && a2 != b2) return 1 / (b2 - a2);
        return 0;
    };

    private AntiNoise antiNoiseOfArithmeticMean = (f) -> {
        for (int i = 0; i < f.length; i++) {
            for (int j = 0; j < f[0].length; j++) {
                double average = 0;
                int a, b, c, d, t = 0;
                if (j - heightMaskSlider.getValue() >= 0) a = (int) (j - heightMaskSlider.getValue());
                else a = 0;
                if (j + heightMaskSlider.getValue() <= f[0].length) b = (int) (j + heightMaskSlider.getValue());
                else b = f[0].length;
                if (i - widthMaskSlider.getValue() >= 0) c = (int) (i - widthMaskSlider.getValue());
                else c = 0;
                if (i + widthMaskSlider.getValue() <= f.length) d = (int) (i + widthMaskSlider.getValue());
                else d = f.length;
                for (int k = c; k < d; k++) {
                    for (int l = a; l < b; l++) {
                        average += f[k][l];
                        t++;
                    }
                }
                average /= t;
                f[i][j] = average;
            }
        }
    };
    private AntiNoise antiNoiseOfHarmonicMean = (f) -> {
        for (int i = 0; i < f.length; i++) {
            for (int j = 0; j < f[0].length; j++) {
                double average = 0;
                int a, b, c, d, t = 0;
                if (j - heightMaskSlider.getValue() >= 0) a = (int) (j - heightMaskSlider.getValue());
                else a = 0;
                if (j + heightMaskSlider.getValue() <= f[0].length) b = (int) (j + heightMaskSlider.getValue());
                else b = f[0].length;
                if (i - widthMaskSlider.getValue() >= 0) c = (int) (i - widthMaskSlider.getValue());
                else c = 0;
                if (i + widthMaskSlider.getValue() <= f.length) d = (int) (i + widthMaskSlider.getValue());
                else d = f.length;
                for (int k = c; k < d; k++) {
                    for (int l = a; l < b; l++) {
                        if (f[k][l] != 0)
                            average += 1d / f[k][l];
                        t++;
                    }
                }
                average = t / average;
                f[i][j] = average;
            }
        }
    };

    private Brightness brightness = i -> {
        double per;
        if (brightnessSlider.getValue() >= 0) per = (255 - i) / 100d;
        else per = (i - 0) / 100d;
        return i + brightnessSlider.getValue() * per;
    };

    @Nullable
    private File frequencyFiltering(File file, BiFunction<Double, Double, Double> filter) {
        File result = null;
        try {
            BufferedImage srcImage = ImageIO.read(file);
            int width = srcImage.getWidth();
            int height = srcImage.getHeight();
            int widthPow2 = 2 * Integer.highestOneBit(width);
            int heightPow2 = 2 * Integer.highestOneBit(height);
            DoubleFFT_2D doubleFFT_2D = new DoubleFFT_2D(widthPow2, heightPow2);
            double red[][] = new double[widthPow2][heightPow2];
            double green[][] = new double[widthPow2][heightPow2];
            double blue[][] = new double[widthPow2][heightPow2];
            //TODO добавить alpha?
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    Color color = new Color(srcImage.getRGB(i, j));
                    red[i][j] = color.getRed();
                    green[i][j] = color.getGreen();
                    blue[i][j] = color.getBlue();
                }
            }
            //1//////////////////////////////////////////////
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    red[x][y] *= Math.pow(-1, x + y);
                    green[x][y] *= Math.pow(-1, x + y);
                    blue[x][y] *= Math.pow(-1, x + y);
                }
            }
            //////////////////////////////////////////////////
            //2///////////////////////////////////////////////
            doubleFFT_2D.realForward(red);
            doubleFFT_2D.realForward(green);
            doubleFFT_2D.realForward(blue);
            //////////////////////////////////////////////////
            double[][] H = new double[width][height];
            double radiusRed = getRadius(red, alphaSlider.getValue() / 100);
            double radiusGreen = getRadius(green, alphaSlider.getValue() / 100);
            double radiusBlue = getRadius(blue, alphaSlider.getValue() / 100);
            double d0 = Math.max(Math.max(radiusRed, radiusGreen), radiusBlue);
            for (int u = 0; u < width; u++) {
                for (int v = 0; v < height; v++) {
                    double d = Math.sqrt((u - width / 2d) * (u - width / 2d) + (v - height / 2d) * (v - height / 2d));
                    H[u][v] = filter.apply(d, d0);
                }
            }
            //3//////////////////////////////////////////////
            for (int u = 0; u < width; u++) {
                for (int v = 0; v < height; v++) {
                    red[u][v] *= H[u][v];
                    green[u][v] *= H[u][v];
                    blue[u][v] *= H[u][v];
                }
            }
            //////////////////////////////////////////////////
            //4//////////////////////////////////////////////
            doubleFFT_2D.realInverse(red, true);
            doubleFFT_2D.realInverse(green, true);
            doubleFFT_2D.realInverse(blue, true);
            //////////////////////////////////////////////////
            //5//////////////////////////////////////////////
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    red[x][y] *= Math.pow(-1, x + y);
                    green[x][y] *= Math.pow(-1, x + y);
                    blue[x][y] *= Math.pow(-1, x + y);
                }
            }
            //////////////////////////////////////////////////
            //6//////////////////////////////////////////////
            BufferedImage resImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    Color color = new Color((Math.abs((int) red[x][y])) % 256, (Math.abs((int) green[x][y])) % 256, (Math.abs((int) blue[x][y])) % 256);
                    resImage.setRGB(x, y, color.getRGB());
                }
            }
            Random random = new Random();
            result = new File(random.nextLong() + "." + FilenameUtils.getExtension(file.toString()));
            FileOutputStream fileOutputStream = new FileOutputStream(result);
            ImageIO.write(resImage, FilenameUtils.getExtension(file.toString()), fileOutputStream);
            fileOutputStream.close();
            //////////////////////////////////////////////////
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Nullable
    private File applyingNoise(File file, Noise noise) {
        File result = null;
        try {
            BufferedImage srcImage = ImageIO.read(file);
            int width = srcImage.getWidth();
            int height = srcImage.getHeight();
            double red[][] = new double[width][height];
            double green[][] = new double[width][height];
            double blue[][] = new double[width][height];
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    Color color = new Color(srcImage.getRGB(i, j));
                    red[i][j] = color.getRed();
                    green[i][j] = color.getGreen();
                    blue[i][j] = color.getBlue();
                }
            }
            double redMathExpectation = mathExpectation(red);
            double greenMathExpectation = mathExpectation(green);
            double blueMathExpectation = mathExpectation(blue);
            double redDispersion = dispersion(red, redMathExpectation);
            double greenDispersion = dispersion(green, greenMathExpectation);
            double blueDispersion = dispersion(blue, blueMathExpectation);
            Random random = new Random();
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    if (noise.applyNoise(red[i][j], redMathExpectation, redDispersion) < random.nextGaussian() &&
                            noise.applyNoise(green[i][j], greenMathExpectation, greenDispersion) < random.nextGaussian() &&
                            noise.applyNoise(blue[i][j], blueMathExpectation, blueDispersion) < random.nextGaussian()) {
                        if (random.nextInt(2) == 0)
                            red[i][j] += noiseSlider.getValue();
                        else red[i][j] -= noiseSlider.getValue();
                        if (random.nextInt(2) == 0)
                            green[i][j] += noiseSlider.getValue();
                        else green[i][j] -= noiseSlider.getValue();
                        if (random.nextInt(2) == 0)
                            blue[i][j] += noiseSlider.getValue();
                        else blue[i][j] -= noiseSlider.getValue();
                    }
                }
            }
            BufferedImage resImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    Color color = new Color((Math.abs((int) red[x][y])) % 256, (Math.abs((int) green[x][y])) % 256, (Math.abs((int) blue[x][y])) % 256);
                    resImage.setRGB(x, y, color.getRGB());
                }
            }
            result = new File(random.nextLong() + "." + FilenameUtils.getExtension(file.toString()));
            FileOutputStream fileOutputStream = new FileOutputStream(result);
            ImageIO.write(resImage, FilenameUtils.getExtension(file.toString()), fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Nullable
    private File applyingAntiNoise(File file, AntiNoise antiNoise) {
        File result = null;
        try {
            BufferedImage srcImage = ImageIO.read(file);
            int width = srcImage.getWidth();
            int height = srcImage.getHeight();
            double red[][] = new double[width][height];
            double green[][] = new double[width][height];
            double blue[][] = new double[width][height];
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    Color color = new Color(srcImage.getRGB(i, j));
                    red[i][j] = color.getRed();
                    green[i][j] = color.getGreen();
                    blue[i][j] = color.getBlue();
                }
            }
            antiNoise.applyAntiNoise(red);
            antiNoise.applyAntiNoise(green);
            antiNoise.applyAntiNoise(blue);
            BufferedImage resImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    Color color = new Color((Math.abs((int) red[x][y])) % 256, (Math.abs((int) green[x][y])) % 256, (Math.abs((int) blue[x][y])) % 256);
                    resImage.setRGB(x, y, color.getRGB());
                }
            }
            Random random = new Random();
            result = new File(random.nextLong() + "." + FilenameUtils.getExtension(file.toString()));
            FileOutputStream fileOutputStream = new FileOutputStream(result);
            ImageIO.write(resImage, FilenameUtils.getExtension(file.toString()), fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Nullable
    private File applyingBrightness(File file, Brightness brightness) {
        File result = null;
        try {
            BufferedImage srcImage = ImageIO.read(file);
            int width = srcImage.getWidth();
            int height = srcImage.getHeight();
            double red[][] = new double[width][height];
            double green[][] = new double[width][height];
            double blue[][] = new double[width][height];
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    Color color = new Color(srcImage.getRGB(i, j));
                    red[i][j] = brightness.applyBrightness(color.getRed());
                    green[i][j] = brightness.applyBrightness(color.getGreen());
                    blue[i][j] = brightness.applyBrightness(color.getBlue());
                }
            }
            BufferedImage resImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    Color color = new Color((Math.abs((int) red[x][y])) % 256, (Math.abs((int) green[x][y])) % 256, (Math.abs((int) blue[x][y])) % 256);
                    resImage.setRGB(x, y, color.getRGB());
                }
            }
            Random random = new Random();
            result = new File(random.nextLong() + "." + FilenameUtils.getExtension(file.toString()));
            FileOutputStream fileOutputStream = new FileOutputStream(result);
            ImageIO.write(resImage, FilenameUtils.getExtension(file.toString()), fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Nullable
    private File applyingContrast(File file) {
        File result = null;
        try {
            BufferedImage srcImage = ImageIO.read(file);
            int width = srcImage.getWidth();
            int height = srcImage.getHeight();
            double red[][] = new double[width][height];
            double green[][] = new double[width][height];
            double blue[][] = new double[width][height];
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    Color color = new Color(srcImage.getRGB(i, j));
                    red[i][j] = color.getRed();
                    green[i][j] = color.getGreen();
                    blue[i][j] = color.getBlue();
                }
            }
            double k = 1d + contrastSlider.getValue() / 100d;
            double iab = imageAverageBrightness(red, green, blue), delta;
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    delta = red[i][j] - iab;
                    red[i][j] = iab + k * delta;
                    delta = green[i][j] - iab;
                    green[i][j] = iab + k * delta;
                    delta = blue[i][j] - iab;
                    blue[i][j] = iab + k * delta;
                }
            }
            BufferedImage resImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    Color color = new Color((Math.abs((int) red[x][y])) % 256, (Math.abs((int) green[x][y])) % 256, (Math.abs((int) blue[x][y])) % 256);
                    resImage.setRGB(x, y, color.getRGB());
                }
            }
            Random random = new Random();
            result = new File(random.nextLong() + "." + FilenameUtils.getExtension(file.toString()));
            FileOutputStream fileOutputStream = new FileOutputStream(result);
            ImageIO.write(resImage, FilenameUtils.getExtension(file.toString()), fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Nullable
    private File applyingColorScheme(File file) {
        File result = null;
        try {
            BufferedImage srcImage = ImageIO.read(file);
            int width = srcImage.getWidth();
            int height = srcImage.getHeight();
            double red[][] = new double[width][height];
            double green[][] = new double[width][height];
            double blue[][] = new double[width][height];
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    Color color = new Color(srcImage.getRGB(i, j));
                    red[i][j] = color.getRed();
                    green[i][j] = color.getGreen();
                    blue[i][j] = color.getBlue();
                }
            }
            double perRed, perGreen, perBlue;
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    perRed = (255 - red[i][j]) / 100d;
                    perGreen = (255 - green[i][j]) / 100d;
                    perBlue = (255 - blue[i][j]) / 100d;
                    red[i][j] += perRed * redSlider.getValue();
                    green[i][j] += perGreen * greenSlider.getValue();
                    blue[i][j] += perBlue * blueSlider.getValue();
                }
            }
            BufferedImage resImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    Color color = new Color((Math.abs((int) red[x][y])) % 256, (Math.abs((int) green[x][y])) % 256, (Math.abs((int) blue[x][y])) % 256);
                    resImage.setRGB(x, y, color.getRGB());
                }
            }
            Random random = new Random();
            result = new File(random.nextLong() + "." + FilenameUtils.getExtension(file.toString()));
            FileOutputStream fileOutputStream = new FileOutputStream(result);
            ImageIO.write(resImage, FilenameUtils.getExtension(file.toString()), fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alphaSlider = new Slider();
        alphaSlider.setMin(0);
        alphaSlider.setMax(100);
        alphaSlider.setValue(50);
        alphaSlider.setShowTickLabels(true);
        alphaSlider.setShowTickMarks(true);
        alphaSlider.setMajorTickUnit(10);
        alphaSlider.setMinorTickCount(1);

        nSlider = new Slider();
        nSlider.setMin(1);
        nSlider.setMax(10);
        nSlider.setValue(1);
        nSlider.setShowTickLabels(true);
        nSlider.setShowTickMarks(true);
        nSlider.setBlockIncrement(1);
        nSlider.setMajorTickUnit(1);
        nSlider.setMinorTickCount(1);

        wSlider = new Slider();
        wSlider.setMin(0);
        wSlider.setMax(500);
        wSlider.setValue(250);
        wSlider.setShowTickLabels(true);
        wSlider.setShowTickMarks(true);
        wSlider.setMajorTickUnit(100);

        noiseSlider = new Slider();
        noiseSlider.setMin(0);
        noiseSlider.setMax(100);
        noiseSlider.setValue(50);
        noiseSlider.setMajorTickUnit(20);
        noiseSlider.setMinorTickCount(1);
        noiseSlider.setShowTickLabels(true);
        noiseSlider.setShowTickMarks(true);

        widthMaskSlider = new Slider();
        widthMaskSlider.setMin(1);
        widthMaskSlider.setMax(5);
        widthMaskSlider.setValue(3);
        widthMaskSlider.setMajorTickUnit(1);
        widthMaskSlider.setMinorTickCount(0);
        widthMaskSlider.setShowTickLabels(true);
        widthMaskSlider.setShowTickMarks(true);
        widthMaskSlider.valueProperty().addListener((obs, oldVal, newVal) ->
                widthMaskSlider.setValue(newVal.intValue()));

        heightMaskSlider = new Slider();
        heightMaskSlider.setMin(1);
        heightMaskSlider.setMax(5);
        heightMaskSlider.setValue(3);
        heightMaskSlider.setMajorTickUnit(1);
        heightMaskSlider.setMinorTickCount(0);
        heightMaskSlider.setShowTickLabels(true);
        heightMaskSlider.setShowTickMarks(true);
        heightMaskSlider.valueProperty().addListener((obs, oldVal, newVal) ->
                heightMaskSlider.setValue(newVal.intValue()));

        brightnessSlider = new Slider();
        brightnessSlider.setMin(-100);
        brightnessSlider.setMax(100);
        brightnessSlider.setValue(0);
        brightnessSlider.setMajorTickUnit(20);
        brightnessSlider.setMinorTickCount(0);
        brightnessSlider.setShowTickLabels(true);
        brightnessSlider.setShowTickMarks(true);

        contrastSlider = new Slider();
        contrastSlider.setMin(-100);
        contrastSlider.setMax(100);
        contrastSlider.setValue(0);
        contrastSlider.setMajorTickUnit(20);
        contrastSlider.setMinorTickCount(0);
        contrastSlider.setShowTickLabels(true);
        contrastSlider.setShowTickMarks(true);

        redSlider = new Slider();
        redSlider.setMin(0);
        redSlider.setMax(100);
        redSlider.setValue(0);
        redSlider.setMajorTickUnit(20);
        redSlider.setMinorTickCount(1);
        redSlider.setShowTickLabels(true);
        redSlider.setShowTickMarks(true);

        greenSlider = new Slider();
        greenSlider.setMin(0);
        greenSlider.setMax(100);
        greenSlider.setValue(0);
        greenSlider.setMajorTickUnit(20);
        greenSlider.setMinorTickCount(1);
        greenSlider.setShowTickLabels(true);
        greenSlider.setShowTickMarks(true);

        blueSlider = new Slider();
        blueSlider.setMin(0);
        blueSlider.setMax(100);
        blueSlider.setValue(0);
        blueSlider.setMajorTickUnit(20);
        blueSlider.setMinorTickCount(1);
        blueSlider.setShowTickLabels(true);
        blueSlider.setShowTickMarks(true);

        menuBar.prefWidthProperty().bind(pane.widthProperty());

        imageView.fitWidthProperty().bind(pane.widthProperty());
        imageView.fitHeightProperty().bind(pane.heightProperty());
        imageView.layoutYProperty().bind(menuBar.heightProperty());
        openMenuItem.setOnAction((event) -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Выбрать изображение");
            fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("GIF", "*.gif"),
                    new FileChooser.ExtensionFilter("BMP", "*.bmp"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );
            File chooseImage = fileChooser.showOpenDialog(pane.getScene().getWindow());
            if (chooseImage != null) {
                currentImage = chooseImage;
                imageView.setImage(new Image(currentImage.toURI().toString()));
            }
        });

        lowPassGaussMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultFrequencyFiltering("Результат фильтрации фильтром низких частот Гаусса", lowPassGauss);
        });

        lowPassButterworthMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultFrequencyFiltering("Результат фильтрации фильтром низких частот Баттерворта", lowPassButterworth);
        });

        highPassGaussMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultFrequencyFiltering("Результат фильтрации фильтром высоких частот Гаусса", highPassGauss);
        });

        highPassButterworthMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultFrequencyFiltering("Результат фильтрации фильтром высоких частот Баттерворта", highPassButterworth);
        });

        rejectButterworthMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultFrequencyFiltering("Результат фильтрации режекторным фильтром Баттерворта", rejectButterworth);
        });

        rejectGaussMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultFrequencyFiltering("Результат фильтрации режекторным фильтром Гаусса", rejectGauss);
        });

        alphaMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Процент энергии");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(pane.getScene().getWindow());
            Pane pane = new Pane();
            alphaSlider.prefWidthProperty().bind(pane.widthProperty());
            alphaSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.5));
            pane.getChildren().add(alphaSlider);
            Scene scene = new Scene(pane, 200, 200);
            stage.setScene(scene);
            stage.show();
        });

        nMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Порядок фильтра");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(pane.getScene().getWindow());
            Pane pane = new Pane();
            nSlider.prefWidthProperty().bind(pane.widthProperty());
            nSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.5));
            pane.getChildren().add(nSlider);
            Scene scene = new Scene(pane, 200, 200);
            stage.setScene(scene);
            stage.show();
        });

        wMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Толщина кольца");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(pane.getScene().getWindow());
            Pane pane = new Pane();
            wSlider.prefWidthProperty().bind(pane.widthProperty());
            wSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.5));
            pane.getChildren().add(wSlider);
            Scene scene = new Scene(pane, 200, 200);
            stage.setScene(scene);
            stage.show();
        });

        noiseMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Уровень шума");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(pane.getScene().getWindow());
            Pane pane = new Pane();
            noiseSlider.prefWidthProperty().bind(pane.widthProperty());
            noiseSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.5));
            pane.getChildren().add(noiseSlider);
            Scene scene = new Scene(pane, 200, 200);
            stage.setScene(scene);
            stage.show();
        });

        maskMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Размеры маски");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(pane.getScene().getWindow());
            Pane pane = new Pane();
            heightMaskSlider.prefWidthProperty().bind(pane.widthProperty());
            heightMaskSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.25));
            heightMaskSlider.setTooltip(new Tooltip("Половина высоты маски"));
            widthMaskSlider.prefWidthProperty().bind(pane.widthProperty());
            widthMaskSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.75));
            widthMaskSlider.setTooltip(new Tooltip("Половина ширины маски"));
            pane.getChildren().addAll(heightMaskSlider, widthMaskSlider);
            Scene scene = new Scene(pane, 200, 200);
            stage.setScene(scene);
            stage.show();
        });

        setBrightnessAndContrastMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Установка яркости и контрастности");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(pane.getScene().getWindow());
            Pane pane = new Pane();
            brightnessSlider.prefWidthProperty().bind(pane.widthProperty());
            brightnessSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.25));
            brightnessSlider.setTooltip(new Tooltip("Яркость"));

            contrastSlider.prefWidthProperty().bind(pane.widthProperty());
            contrastSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.75));
            contrastSlider.setTooltip(new Tooltip("Контрастность"));
            pane.getChildren().addAll(brightnessSlider, contrastSlider);
            Scene scene = new Scene(pane, 200, 200);
            stage.setScene(scene);
            stage.show();
        });

        setColorSchemeMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Установка цветовой схемы");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(pane.getScene().getWindow());
            Pane pane = new Pane();
            redSlider.prefWidthProperty().bind(pane.widthProperty());
            redSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.2));
            redSlider.setTooltip(new Tooltip("Красный"));

            greenSlider.prefWidthProperty().bind(pane.widthProperty());
            greenSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.4));
            greenSlider.setTooltip(new Tooltip("Зелёный"));

            blueSlider.prefWidthProperty().bind(pane.widthProperty());
            blueSlider.layoutYProperty().bind(pane.heightProperty().multiply(0.6));
            blueSlider.setTooltip(new Tooltip("Синий"));

            pane.getChildren().addAll(redSlider, greenSlider, blueSlider);
            Scene scene = new Scene(pane, 200, 200);
            stage.setScene(scene);
            stage.show();
        });

        noiseOfGaussMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultNoise("Результат применения Гауссова шума", noiseOfGauss);
        });

        noiseOfRayleighMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultNoise("Результат применения шума Релея", noiseOfRayleigh);
        });

        noiseOfErlangMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultNoise("Результат применения шума Эрланга (гамма шум)", noiseOfErlang);
        });

        noiseOfExpMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultNoise("Результат применения экспоненциального шума", noiseOfExp);
        });

        noiseOfUniformMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultNoise("Результат применения равномерного шума", noiseOfUniform);
        });

        arithmeticMeanAntiNoiseMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultAntiNoise("Результат применения шумоподавления на основе среднего арифметического", antiNoiseOfArithmeticMean);
        });

        harmonicMeanAntiNoiseMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultAntiNoise("Результат применения шумоподавления на основе среднего гармонического", antiNoiseOfHarmonicMean);
        });

        brightnessMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultBrightness("Результат изменения яркости", brightness);
        });

        contrastMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultContrast("Результат изменения контрастности");
        });

        colorSchemeMenuItem.setOnAction(event -> {
            if (currentImage != null)
                resultColorScheme("Результат применения цветовой схемы");
        });
    }

    /**
     * Метод, вычисляющий матожидание случайных величин из массива
     *
     * @param f массив случайных величин
     * @return матожидание
     */
    private double mathExpectation(double[][] f) {
        double sum = 0;
        for (double[] aF : f) {
            for (int j = 0; j < f[0].length; j++) {
                sum += aF[j];
            }
        }
        return sum / (f.length * f[0].length);
    }

    /**
     * Метод, вычисляющий дисперсию случайных величин из массива
     *
     * @param f массив случайных величин
     * @return дисперсия
     */
    private double dispersion(double[][] f, double mathExpectation) {
        double sum = 0;
        for (double[] aF : f) {
            for (int j = 0; j < f[0].length; j++) {
                sum += ((aF[j] - mathExpectation) * (aF[j] - mathExpectation));
            }
        }
        return sum / (f.length * f[0].length);
    }

    private void resultAntiNoise(String title, AntiNoise antiNoise) {
        Stage stage = new Stage();
        stage.setTitle(title);
        //stage.initModality(Modality.WINDOW_MODAL);
        //stage.initOwner(pane.getScene().getWindow());
        Pane pane = new Pane();

        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("Файл");
        MenuItem saveMenuItem = new MenuItem("Сохранить");
        menuBar.getMenus().add(fileMenu);
        fileMenu.getItems().add(saveMenuItem);
        menuBar.prefWidthProperty().bind(pane.widthProperty());

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setVisible(false);
        progressIndicator.layoutXProperty().bind(pane.widthProperty().multiply(0.5).subtract(25));
        progressIndicator.layoutYProperty().bind(pane.heightProperty().multiply(0.5).subtract(25));

        ImageView imageView = new ImageView();
        imageView.fitWidthProperty().bind(pane.widthProperty());
        imageView.fitHeightProperty().bind(pane.heightProperty());
        imageView.setPreserveRatio(true);

        pane.getChildren().addAll(imageView, progressIndicator, menuBar);
        Scene scene = new Scene(pane, 800, 600);
        stage.setScene(scene);
        Task<File> task = new Task<File>() {
            @Override
            protected File call() throws Exception {
                progressIndicator.setVisible(true);
                File result = applyingAntiNoise(currentImage, antiNoise);
                progressIndicator.setVisible(false);
                imageView.setImage(new Image(result.toURI().toString()));

                saveMenuItem.setOnAction((event) -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Сохранить изображение");
                    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
                    File saveFile = fileChooser.showSaveDialog(pane.getScene().getWindow());
                    if (saveFile != null) {
                        try (RandomAccessFile randomAccessFileResult = new RandomAccessFile(result, "r");
                             RandomAccessFile randomAccessFileSave = new RandomAccessFile(saveFile + "." + FilenameUtils.getExtension(result.toString()), "rw")) {
                            byte[] resultBytes = new byte[(int) randomAccessFileResult.length()];
                            randomAccessFileResult.read(resultBytes);
                            randomAccessFileSave.write(resultBytes);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return result;
            }
        };
        new Thread(task).start();
        stage.setOnCloseRequest(event -> {
            if (task.isDone()) {
                File file = task.getValue();
                if (file != null) file.delete();
            }
            if (task.isRunning()) {
                task.setOnSucceeded(event1 -> {
                    File file = task.getValue();
                    if (file != null) file.delete();
                });
            }
        });
        stage.show();
    }


    private void resultNoise(String title, Noise noise) {
        Stage stage = new Stage();
        stage.setTitle(title);
        //stage.initModality(Modality.WINDOW_MODAL);
        //stage.initOwner(pane.getScene().getWindow());
        Pane pane = new Pane();

        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("Файл");
        MenuItem saveMenuItem = new MenuItem("Сохранить");
        menuBar.getMenus().add(fileMenu);
        fileMenu.getItems().add(saveMenuItem);
        menuBar.prefWidthProperty().bind(pane.widthProperty());

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setVisible(false);
        progressIndicator.layoutXProperty().bind(pane.widthProperty().multiply(0.5).subtract(25));
        progressIndicator.layoutYProperty().bind(pane.heightProperty().multiply(0.5).subtract(25));

        ImageView imageView = new ImageView();
        imageView.fitWidthProperty().bind(pane.widthProperty());
        imageView.fitHeightProperty().bind(pane.heightProperty());
        imageView.setPreserveRatio(true);

        pane.getChildren().addAll(imageView, progressIndicator, menuBar);
        Scene scene = new Scene(pane, 800, 600);
        stage.setScene(scene);
        Task<File> task = new Task<File>() {
            @Override
            protected File call() throws Exception {
                progressIndicator.setVisible(true);
                File result = applyingNoise(currentImage, noise);
                progressIndicator.setVisible(false);
                imageView.setImage(new Image(result.toURI().toString()));

                saveMenuItem.setOnAction((event) -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Сохранить изображение");
                    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
                    File saveFile = fileChooser.showSaveDialog(pane.getScene().getWindow());
                    if (saveFile != null) {
                        try (RandomAccessFile randomAccessFileResult = new RandomAccessFile(result, "r");
                             RandomAccessFile randomAccessFileSave = new RandomAccessFile(saveFile + "." + FilenameUtils.getExtension(result.toString()), "rw")) {
                            byte[] resultBytes = new byte[(int) randomAccessFileResult.length()];
                            randomAccessFileResult.read(resultBytes);
                            randomAccessFileSave.write(resultBytes);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return result;
            }
        };
        new Thread(task).start();
        stage.setOnCloseRequest(event -> {
            if (task.isDone()) {
                File file = task.getValue();
                if (file != null) file.delete();
            }
            if (task.isRunning()) {
                task.setOnSucceeded(event1 -> {
                    File file = task.getValue();
                    if (file != null) file.delete();
                });
            }
        });
        stage.show();
    }

    private void resultFrequencyFiltering(String title, BiFunction<Double, Double, Double> frequencyFilter) {
        Stage stage = new Stage();
        stage.setTitle(title);
        //stage.initModality(Modality.WINDOW_MODAL);
        //stage.initOwner(pane.getScene().getWindow());
        Pane pane = new Pane();

        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("Файл");
        MenuItem saveMenuItem = new MenuItem("Сохранить");
        menuBar.getMenus().add(fileMenu);
        fileMenu.getItems().add(saveMenuItem);
        menuBar.prefWidthProperty().bind(pane.widthProperty());

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setVisible(false);
        progressIndicator.layoutXProperty().bind(pane.widthProperty().multiply(0.5).subtract(25));
        progressIndicator.layoutYProperty().bind(pane.heightProperty().multiply(0.5).subtract(25));

        ImageView imageView = new ImageView();
        imageView.fitWidthProperty().bind(pane.widthProperty());
        imageView.fitHeightProperty().bind(pane.heightProperty());
        imageView.setPreserveRatio(true);

        pane.getChildren().addAll(imageView, progressIndicator, menuBar);
        Scene scene = new Scene(pane, 800, 600);
        stage.setScene(scene);
        Task<File> task = new Task<File>() {
            @Override
            protected File call() throws Exception {
                progressIndicator.setVisible(true);
                File result = frequencyFiltering(currentImage, frequencyFilter);
                progressIndicator.setVisible(false);
                imageView.setImage(new Image(result.toURI().toString()));

                saveMenuItem.setOnAction((event) -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Сохранить изображение");
                    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
                    File saveFile = fileChooser.showSaveDialog(pane.getScene().getWindow());
                    if (saveFile != null) {
                        try (RandomAccessFile randomAccessFileResult = new RandomAccessFile(result, "r");
                             RandomAccessFile randomAccessFileSave = new RandomAccessFile(saveFile + "." + FilenameUtils.getExtension(result.toString()), "rw")) {
                            byte[] resultBytes = new byte[(int) randomAccessFileResult.length()];
                            randomAccessFileResult.read(resultBytes);
                            randomAccessFileSave.write(resultBytes);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return result;
            }
        };
        new Thread(task).start();
        stage.setOnCloseRequest(event -> {
            if (task.isDone()) {
                File file = task.getValue();
                if (file != null) file.delete();
            }
            if (task.isRunning()) {
                task.setOnSucceeded(event1 -> {
                    File file = task.getValue();
                    if (file != null) file.delete();
                });
            }
        });
        stage.show();
    }

    private void resultBrightness(String title, Brightness brightness) {
        Stage stage = new Stage();
        stage.setTitle(title);
        //stage.initModality(Modality.WINDOW_MODAL);
        //stage.initOwner(pane.getScene().getWindow());
        Pane pane = new Pane();

        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("Файл");
        MenuItem saveMenuItem = new MenuItem("Сохранить");
        menuBar.getMenus().add(fileMenu);
        fileMenu.getItems().add(saveMenuItem);
        menuBar.prefWidthProperty().bind(pane.widthProperty());

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setVisible(false);
        progressIndicator.layoutXProperty().bind(pane.widthProperty().multiply(0.5).subtract(25));
        progressIndicator.layoutYProperty().bind(pane.heightProperty().multiply(0.5).subtract(25));

        ImageView imageView = new ImageView();
        imageView.fitWidthProperty().bind(pane.widthProperty());
        imageView.fitHeightProperty().bind(pane.heightProperty());
        imageView.setPreserveRatio(true);

        pane.getChildren().addAll(imageView, progressIndicator, menuBar);
        Scene scene = new Scene(pane, 800, 600);
        stage.setScene(scene);
        Task<File> task = new Task<File>() {
            @Override
            protected File call() throws Exception {
                progressIndicator.setVisible(true);
                File result = applyingBrightness(currentImage, brightness);
                progressIndicator.setVisible(false);
                imageView.setImage(new Image(result.toURI().toString()));

                saveMenuItem.setOnAction((event) -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Сохранить изображение");
                    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
                    File saveFile = fileChooser.showSaveDialog(pane.getScene().getWindow());
                    if (saveFile != null) {
                        try (RandomAccessFile randomAccessFileResult = new RandomAccessFile(result, "r");
                             RandomAccessFile randomAccessFileSave = new RandomAccessFile(saveFile + "." + FilenameUtils.getExtension(result.toString()), "rw")) {
                            byte[] resultBytes = new byte[(int) randomAccessFileResult.length()];
                            randomAccessFileResult.read(resultBytes);
                            randomAccessFileSave.write(resultBytes);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return result;
            }
        };
        new Thread(task).start();
        stage.setOnCloseRequest(event -> {
            if (task.isDone()) {
                File file = task.getValue();
                if (file != null) file.delete();
            }
            if (task.isRunning()) {
                task.setOnSucceeded(event1 -> {
                    File file = task.getValue();
                    if (file != null) file.delete();
                });
            }
        });
        stage.show();
    }

    private void resultContrast(String title) {
        Stage stage = new Stage();
        stage.setTitle(title);
        //stage.initModality(Modality.WINDOW_MODAL);
        //stage.initOwner(pane.getScene().getWindow());
        Pane pane = new Pane();

        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("Файл");
        MenuItem saveMenuItem = new MenuItem("Сохранить");
        menuBar.getMenus().add(fileMenu);
        fileMenu.getItems().add(saveMenuItem);
        menuBar.prefWidthProperty().bind(pane.widthProperty());

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setVisible(false);
        progressIndicator.layoutXProperty().bind(pane.widthProperty().multiply(0.5).subtract(25));
        progressIndicator.layoutYProperty().bind(pane.heightProperty().multiply(0.5).subtract(25));

        ImageView imageView = new ImageView();
        imageView.fitWidthProperty().bind(pane.widthProperty());
        imageView.fitHeightProperty().bind(pane.heightProperty());
        imageView.setPreserveRatio(true);

        pane.getChildren().addAll(imageView, progressIndicator, menuBar);
        Scene scene = new Scene(pane, 800, 600);
        stage.setScene(scene);
        Task<File> task = new Task<File>() {
            @Override
            protected File call() throws Exception {
                progressIndicator.setVisible(true);
                File result = applyingContrast(currentImage);
                progressIndicator.setVisible(false);
                imageView.setImage(new Image(result.toURI().toString()));

                saveMenuItem.setOnAction((event) -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Сохранить изображение");
                    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
                    File saveFile = fileChooser.showSaveDialog(pane.getScene().getWindow());
                    if (saveFile != null) {
                        try (RandomAccessFile randomAccessFileResult = new RandomAccessFile(result, "r");
                             RandomAccessFile randomAccessFileSave = new RandomAccessFile(saveFile + "." + FilenameUtils.getExtension(result.toString()), "rw")) {
                            byte[] resultBytes = new byte[(int) randomAccessFileResult.length()];
                            randomAccessFileResult.read(resultBytes);
                            randomAccessFileSave.write(resultBytes);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return result;
            }
        };
        new Thread(task).start();
        stage.setOnCloseRequest(event -> {
            if (task.isDone()) {
                File file = task.getValue();
                if (file != null) file.delete();
            }
            if (task.isRunning()) {
                task.setOnSucceeded(event1 -> {
                    File file = task.getValue();
                    if (file != null) file.delete();
                });
            }
        });
        stage.show();
    }

    private void resultColorScheme(String title) {
        Stage stage = new Stage();
        stage.setTitle(title);
        //stage.initModality(Modality.WINDOW_MODAL);
        //stage.initOwner(pane.getScene().getWindow());
        Pane pane = new Pane();

        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("Файл");
        MenuItem saveMenuItem = new MenuItem("Сохранить");
        menuBar.getMenus().add(fileMenu);
        fileMenu.getItems().add(saveMenuItem);
        menuBar.prefWidthProperty().bind(pane.widthProperty());

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setVisible(false);
        progressIndicator.layoutXProperty().bind(pane.widthProperty().multiply(0.5).subtract(25));
        progressIndicator.layoutYProperty().bind(pane.heightProperty().multiply(0.5).subtract(25));

        ImageView imageView = new ImageView();
        imageView.fitWidthProperty().bind(pane.widthProperty());
        imageView.fitHeightProperty().bind(pane.heightProperty());
        imageView.setPreserveRatio(true);

        pane.getChildren().addAll(imageView, progressIndicator, menuBar);
        Scene scene = new Scene(pane, 800, 600);
        stage.setScene(scene);
        Task<File> task = new Task<File>() {
            @Override
            protected File call() throws Exception {
                progressIndicator.setVisible(true);
                File result = applyingColorScheme(currentImage);
                progressIndicator.setVisible(false);
                imageView.setImage(new Image(result.toURI().toString()));

                saveMenuItem.setOnAction((event) -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Сохранить изображение");
                    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
                    File saveFile = fileChooser.showSaveDialog(pane.getScene().getWindow());
                    if (saveFile != null) {
                        try (RandomAccessFile randomAccessFileResult = new RandomAccessFile(result, "r");
                             RandomAccessFile randomAccessFileSave = new RandomAccessFile(saveFile + "." + FilenameUtils.getExtension(result.toString()), "rw")) {
                            byte[] resultBytes = new byte[(int) randomAccessFileResult.length()];
                            randomAccessFileResult.read(resultBytes);
                            randomAccessFileSave.write(resultBytes);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return result;
            }
        };
        new Thread(task).start();
        stage.setOnCloseRequest(event -> {
            if (task.isDone()) {
                File file = task.getValue();
                if (file != null) file.delete();
            }
            if (task.isRunning()) {
                task.setOnSucceeded(event1 -> {
                    File file = task.getValue();
                    if (file != null) file.delete();
                });
            }
        });
        stage.show();
    }

    private long fact(int n) {
        long fact = 1;
        for (int i = 2; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }

    /**
     * Метод получения радиуса энергии для массива преобразования Фурье и количестве энергии
     *
     * @param F     массив Фурье
     * @param alpha количество энергии [0,1]
     * @return радиус, в котором сосредоточено 100*alpha% энергии
     */
    private int getRadius(double[][] F, double alpha) {
        double PT = 0, tempAlpha = 0;
        int radius, y0 = F[0].length / 2, x0 = F.length / 2;
        for (int u = 0; u < F.length; u++) {
            for (int v = 0; v < F[0].length; v++) {
                PT += F[u][v] * F[u][v];
            }
        }

        /*label:
        for (radius = 0; radius < Math.min(x0, y0); radius++) {
            tempAlpha = 0;
            for (int u = x0 - radius; u < x0 + radius; u++) {
                for (int v = y0 - radius; v < y0 + radius; v++) {
                    if (u * u + v * v <= (radius + x0) * (radius + y0)) {
                        tempAlpha += (F[u][v] * F[u][v] / PT);
                        if (tempAlpha >= alpha) break label;
                    }
                }
            }
        }*/

        label:
        for (radius = 0; radius < Math.min(x0, y0); radius++) {
            for (int v = y0 - radius; v < y0 + radius; v++) {
                tempAlpha += (F[x0 - radius][v] * F[x0 - radius][v] / PT);
                if (tempAlpha >= alpha) break label;
            }
            for (int v = y0 - radius; v < y0 + radius; v++) {
                tempAlpha += (F[x0 + radius][v] * F[x0 + radius][v] / PT);
                if (tempAlpha >= alpha) break label;
            }
            for (int u = x0 - radius; u < x0 + radius; u++) {
                tempAlpha += (F[u][y0 - radius] * F[u][y0 - radius] / PT);
                if (tempAlpha >= alpha) break label;
            }
            for (int u = x0 - radius; u < x0 + radius; u++) {
                tempAlpha += (F[u][y0 + radius] * F[u][y0 + radius] / PT);
                if (tempAlpha >= alpha) break label;
            }
        }
        return radius;
    }

    private double imageAverageBrightness(double[][] red, double[][] green, double[][] blue) {
        double iab = 0;
        for (int i = 0; i < red.length; i++) {
            for (int j = 0; j < red[0].length; j++) {
                iab += (red[i][j] * 0.299 + green[i][j] * 0.587 + blue[i][j] * 0.114);
            }
        }
        iab /= (red.length * red[0].length);
        return iab;
    }

}
