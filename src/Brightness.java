/**
 * Created by Dmitriy on 30.04.2017.
 */
@FunctionalInterface
public interface Brightness {
    double applyBrightness(double i);
}
