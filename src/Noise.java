/**
 * Created by Dmitriy on 29.04.2017.
 */
@FunctionalInterface
public interface Noise {
    /**
     * Метод применения шума
     *
     * @param z случайная величина (яркость пикселя)
     * @param m матожидание
     * @param d дисперсия
     * @return вероятность появления данного шума
     */
    double applyNoise(double z, double m, double d);
}
