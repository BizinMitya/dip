/**
 * Created by Dmitriy on 29.04.2017.
 */
@FunctionalInterface
public interface AntiNoise {
    void applyAntiNoise(double[][] f);
}
